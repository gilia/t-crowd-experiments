#!/bin/bash

# @parameter1: main folder to put the results.
# @parameter2: experiment configuration file

# @return: one new folder will be created inside of the main folder with the respective results of each execution.

# combinations 2400

# amount 100 tbox
# values 24

# size: 5, 10, 15
# Lc: 5, 10, 15 (Lc its 11 values)
# N: 1, 3, 5
# qm: 2, 5
# pr: 0.1, 0.3, 0.7, 0.9
# pt: 0.5, 0.7, 0.9

#SAT using each solver
#time and result
#time for each execution and the final time


#TODO
# 1 - Check for float numbers for probabilities in java
# 2 - Add other reasoners for purefuture
# 3 - See reporting


if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi

    # number of iterations

	re='^[0-9]+$'
	
	if ! [[ $2 =~ $re ]]; 
	then
   		echo "Number of iterations must be given"; 
   		exit 1
	fi


   	mainfolder="${1}"

    if [ ! -d "$mainfolder" ];
    then
        echo -e "\\e[0;41m $mainfolder does not exist in your filesystem. Exit!\\e[0m"
        exit
    else
        echo -e "Current directory: $mainfolder"
    fi

    if ls ${1}*.config &>/dev/null
    then
        echo "Found."
        configfile=${1}*.config
        echo $configfile
    else
        echo "Not found."
    fi
    
    i=1
    while IFS= read -r line
    do
        if [[ $i =~ 1 ]];
        then
            size=($line)
        fi

        if [[ $i =~ 2 ]];
        then
            lc=($line)
        fi

        if [[ $i =~ 3 ]];
        then
            N=($line)
        fi

        if [[ $i =~ 4 ]];
        then
            qm=($line)
        fi

        if [[ $i =~ 5 ]];
        then
            pr=($line)
        fi

        if [[ $i =~ 6 ]];
        then
            pt=($line)
        fi

        i=`expr $i + 1`
    done < $configfile

    
    echo -e "***********************NuSMV pure future ltl vs. NuSMV pltl"
    echo -e "\n \n"

    ########## Total time
    wholeteststart=$(($(date +%s%N)/1000000))
    echo -e `date`
    ########## Total time


    echo -e "******NuSMV pure future ltl execution"
    echo -e "\n"

    pfltlteststart=$(($(date +%s%N)/1000000))


    ### Reporting

    namefile="result.csv"
    touch $mainfolder/$namefile
    echo "name,size,lc,n,qm,pr,pt,tbox2qtl(s),abox2qtl(s),qtl2qtln(s),abox2qtln(s),qtl2ltl(s),qtln2ltl(s),trtotal(s),sattime(s),total(s),#prop,sat/unsat" >> $mainfolder/$namefile

    ####


# numbers of iterations

counter=1

while [ "$counter" -le "$2" ]; 
do

    echo -e "Iteration number: $counter"
    echo -e ""
    echo -e ""

    # random for iteration
    for j in "${size[@]}"  # Size
    do
        for k in "${lc[@]}" # Lc
        do
            for l in "${N[@]}" # N
            do
                for m in "${qm[@]}" # Qm
                do
                    for p in "${pr[@]}" # Pr
                    do
                        for q in "${pt[0]}" #Pt
                        do
                            
                  ########### NuSMV pure future LTL
                            # New folder to copy the experiment result for this instance
                            
                            namefolder="NuSMV_PF_ltl_iter_${counter}_param_${j}_${k}_${l}_${m}_${p}_${q}"
                            mkdir $mainfolder$namefolder

                            touch $mainfolder$namefolder/abox.json
                            touch $mainfolder$namefolder/outputTDLReasoner.txt    
                            touch $mainfolder$namefolder/outputSATsolver.txt
                            outputSolverfile="$mainfolder$namefolder/outputSATsolver.txt"
                            outputTDLfile="$mainfolder$namefolder/outputTDLReasoner.txt"    
                                        
                                        echo -e "------------------------"
                                        echo -e "Random parameters-> size: ${j} - lc: ${k} - N: ${l} - Qm: ${m} - Pr: ${p} - Pt: ${q}"

                                        iterationstart=$(($(date +%s%N)/1000000))


                            # Invoking TDL-Reasoner with the random parameters, empty abox. The output is logged in a txt file named 'outputTDLReasoner.txt'
                            java -cp t-crowd-cli-4.0.0-SNAPSHOT.jar it.gilia.tcrowd.cli.TCrowd RandomTBoxABoxSatLTL -ltbox $j -n $l -lc $k -qm $m -pt $p -pr $q -a "$mainfolder$namefolder/abox.json" -s NuSMV > $outputTDLfile

                                        translationend=$(($(date +%s%N)/1000000))
                                        translationtotal=$((translationend-iterationstart))

                            file="$mainfolder$namefolder/random.smv"

                                        solverstart=$(($(date +%s%N)/1000000))
    
                            # Run the NuSVM solver. NuSMV flags should be set here. The output is copied to a txt file named 'outputSAT.txt'
                            if [ -f "$file" ];
   	                        then

                                    if [[ "$OSTYPE" == "linux-gnu" ]]; then
            
                                        ./solvers/NuSMV/NuSMV -dcx -bmc -bmc_length 11 $file > $outputSolverfile
        
                                    elif [[ "$OSTYPE" == "darwin"* ]]; then  # Mac OSX

                                        ./solvers/NuSMV-macos/NuSMV -dcx -bmc -bmc_length 11 $file > $outputSolverfile

                                    elif [[ "$OSTYPE" == "cygwin" ]]; then #Windows

	                                    /solvers/NuSMV-win/NuSMV.exe -dcx -bmc -bmc_length 11 $file > $outputSolverfile
                                    fi

                                        solverend=$(($(date +%s%N)/1000000))
                                        solvertotal=$((solverend-solverstart))


                                        #Generate input for reporting

                                        # Reading stats file from library
                                        if ls $mainfolder$namefolder/*.stats &>/dev/null
                                        then
                                            echo "Found."
                                            statsfile=$mainfolder$namefolder/*.stats

                                            i=1
                                            while IFS= read -r line
                                            do
                                                if [[ $i =~ 1 ]];
                                                then
                                                    tbox2qlt1_line=($line)
                                                    idx=`expr ${#tbox2qlt1_line[@]} - 1`
                                                    tbox2qlt1_ms=${tbox2qlt1_line[$idx]}
                                                    tbox2qlt1_s=$(echo "scale=4;$tbox2qlt1_ms / 1000" | bc -l)
                                                fi

                                                if [[ $i =~ 2 ]];
                                                then
                                                    qtl12qtln_line=($line)
                                                    idx=`expr ${#qtl12qtln_line[@]} - 1`
                                                    qtl12qtln_ms=${qtl12qtln_line[$idx]}
                                                    qtl12qtln_s=$(echo "scale=4;$qtl12qtln_ms / 1000" | bc -l)
                                                fi

                                                if [[ $i =~ 3 ]];
                                                then
                                                    qtlN2ltl_line=($line)
                                                    idx=`expr ${#qtlN2ltl_line[@]} - 1`
                                                    qtlN2ltl_ms=${qtlN2ltl_line[$idx]}
                                                    qtlN2ltl_s=$(echo "scale=4;$qtlN2ltl_ms / 1000" | bc -l)
                                                fi

                                                if [[ $i =~ 4 ]];
                                                then
                                                    nprop_line=($line)
                                                    idx=`expr ${#nprop_line[@]} - 1 `
                                                    nprop=${nprop_line[$idx]}
                                                fi

                                                i=`expr $i + 1`
                                            done < $statsfile

                                            # Sat/Unsat
                                            if grep -q "true" $outputSolverfile
                                            then
                                                sat=`echo unsat`
                                            else
                                                if grep -q "false" $outputSolverfile
                                                then
                                                   sat=`echo sat`
                                                else
                                                   sat=`echo undefined`
                                                fi
                                            fi

                                            # Time to seconds

                                            translationtotal_ms=$(($tbox2qlt1_ms+$qtl12qtln_ms+$qtlN2ltl_ms))
                                            translationtotal_s=$(echo "scale=4;$translationtotal_ms / 1000" | bc -l)

                                            solvertotals=$(echo "scale=4;$solvertotal / 1000" | bc -l)

                                            total_ms=$(($solvertotal+$translationtotal_ms))
                                            totals=$(echo "scale=4;$total_ms / 1000" | bc -l)

                                        else
                                        echo "Not found."
                                        fi

                                        echo -e "${namefolder},${j},${k},${l},${m},${p},${q},${tbox2qlt1_s},0,${qtl12qtln_s},0,0,${qtlN2ltl_s},${translationtotal_s},${solvertotals},${totals},${nprop},${sat}" >> $mainfolder/$namefile

                                        ####

                            else
	                            echo "$file not found."
                                exit
                            fi
                        done
                    done
                done
            done
        done
    done

    pfltltestend=$(($(date +%s%N)/1000000))

    pfwholetestruntime=$((pfltltestend-pfltlteststart))

    echo -e ""
    echo -e ""
    
    echo -e "******NuSMV pure future ltl execution"
    echo -e "\n"

    pltlteststart=$(($(date +%s%N)/1000000))

    for j in "${size[@]}"  # Size
    do
        for k in "${lc[@]}" # Lc
        do
            for l in "${N[@]}" # N
            do
                for m in "${qm[@]}" # Qm
                do
                    for p in "${pr[@]}" # Pr
                    do
                        for q in "${pt[0]}" #Pt
                        do
                            
                  ########### NuSMV PLTL
                            # New folder to copy the experiment result for this instance
                            
                            namefolder="NuSMV_pltl_iter_${counter}_param_${j}_${k}_${l}_${m}_${p}_${q}"
                            mkdir $mainfolder$namefolder

                            touch $mainfolder$namefolder/abox.json
                            touch $mainfolder$namefolder/outputTDLReasoner.txt    
                            touch $mainfolder$namefolder/outputSATsolver.txt
                            outputSolverfile="$mainfolder$namefolder/outputSATsolver.txt"
                            outputTDLfile="$mainfolder$namefolder/outputTDLReasoner.txt"

                                        echo -e "------------------------"
                                        echo -e "Random parameters-> size: ${j} - lc: ${k} - N: ${l} - Qm: ${m} - Pr: ${p} - Pt: ${q}"
                                        iterationstart=$(($(date +%s%N)/1000000))    
                            
                            # Invoking TDL-Reasoner with the random parameters, empty abox. The output is logged in a txt file named 'outputTDLReasoner.txt'
                            java -cp t-crowd-cli-4.0.0-SNAPSHOT.jar it.gilia.tcrowd.cli.TCrowd RandomTBoxABoxSatPLTL -ltbox $j -n $l -lc $k -qm $m -pt $p -pr $q -a "$mainfolder$namefolder/abox.json" > $outputTDLfile

                                        translationend=$(($(date +%s%N)/1000000))
                                        translationtotal=$((translationend-iterationstart))

                            file="$mainfolder$namefolder/random.smv"

                                        solverstart=$(($(date +%s%N)/1000000))
    
                            # Run the NuSVM solver. NuSMV flags should be set here. The output is copied to a txt file named 'outputSAT.txt'
                            if [ -f "$file" ];
   	                        then

                                    if [[ "$OSTYPE" == "linux-gnu" ]]; then
            
                                        ./solvers/NuSMV/NuSMV -dcx -bmc -bmc_length 11 $file > $outputSolverfile
        
                                    elif [[ "$OSTYPE" == "darwin"* ]]; then  # Mac OSX

                                        ./solvers/NuSMV-macos/NuSMV -dcx -bmc -bmc_length 11 $file > $outputSolverfile

                                    elif [[ "$OSTYPE" == "cygwin" ]]; then #Windows

	                                    /solvers/NuSMV-win/NuSMV.exe -dcx -bmc -bmc_length 11 $file > $outputSolverfile
                                    fi

                                        solverend=$(($(date +%s%N)/1000000))
                                        solvertotal=$((solverend-solverstart))

                                        #Generate input for reporting

                                        # Reading stats file from library
                                        if ls $mainfolder$namefolder/*.stats &>/dev/null
                                        then
                                            echo "Found."
                                            statsfile=$mainfolder$namefolder/*.stats

                                            i=1
                                            while IFS= read -r line
                                            do
                                                if [[ $i =~ 1 ]];
                                                then
                                                    tbox2qlt1_line=($line)
                                                    idx=`expr ${#tbox2qlt1_line[@]} - 1`
                                                    tbox2qlt1_ms=${tbox2qlt1_line[$idx]}
                                                    tbox2qlt1_s=$(echo "scale=4;$tbox2qlt1_ms / 1000" | bc -l) 
                                                fi

                                                if [[ $i =~ 2 ]];
                                                then
                                                    qtl2ltl_line=($line)
                                                    idx=`expr ${#qtl2ltl_line[@]} - 1`
                                                    qtl2ltl_ms=${qtl2ltl_line[$idx]}
                                                    qtl2ltl_s=$(echo "scale=4;$qtl2ltl_ms / 1000" | bc -l)
                                                fi

                                                if [[ $i =~ 3 ]];
                                                then
                                                    nprop_line=($line)
                                                    idx=`expr ${#nprop_line[@]} - 1`
                                                    nprop=${nprop_line[$idx]}
                                                fi

                                                i=`expr $i + 1`
                                            done < $statsfile



                                            # sat/unsat

                                            if grep -q "true" $outputSolverfile
                                            then
                                                sat=`echo unsat`
                                            else
                                                if grep -q "false" $outputSolverfile
                                                then
                                                   sat=`echo sat`
                                                else
                                                   sat=`echo undefined`
                                                fi
                                            fi

                                            # Time to seconds

                                            translationtotal_ms=$(($tbox2qlt1_ms+$qtl2ltl_ms))
                                            translationtotal_s=$(echo "scale=4;$translationtotal_ms / 1000" | bc -l)

                                            solvertotals=$(echo "scale=4;$solvertotal / 1000" | bc -l)

                                            total_ms=$(($solvertotal+$translationtotal_ms))
                                            totals=$(echo "scale=4;$total_ms / 1000" | bc -l)

                                        else
                                        echo "Not found."
                                        fi

                                        echo -e "${namefolder},${j},${k},${l},${m},${p},${q},${tbox2qlt1_s},0,0,0,${qtl2ltl_s},0,${translationtotal_s},${solvertotals},${totals},${nprop},${sat}" >> $mainfolder/$namefile

                                        ####


                            else
	                            echo "$file not found."
                                exit
                            fi
                        done
                    done
                done
            done
        done
    done

    pltltestend=$(($(date +%s%N)/1000000))

    pltlwholetestruntime=$((pltltestend-pltlteststart))

    echo -e ""
    echo -e ""

    ########## Total time
    wholetestend=$(($(date +%s%N)/1000000))
    echo `date`

    wholetestruntime=$((wholetestend-wholeteststart))

    if [[ $wholetestruntime -gt 1000 ]]
    then
        echo -e "\\e[0;42mComplete Test time (s):\\e[0m"
        echo "scale=2; $wholetestruntime / 1000" | bc -l
    else
        echo -e "\\e[0;42mComplete Test time (ms):\\e[0m" $wholetestruntime
    fi

counter=$(($counter + 1))
done

